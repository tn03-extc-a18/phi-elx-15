/*
	Mathematical Expression Plotter
*/

#include <stdio.h>                                          
#include <stdlib.h>     
#include <math.h>
#include <app_sysinit.h>           

#include <pcf/ui/ui.h>   
#include <pcf/graphics/graphics.h>  
#include <pcf/ui/mainform/mainform.h>         
#include <pcf/ui/control/control.h>        

#include <pcf/ui/textedit/textedit.h>         
#include <pcf/ui/label/label.h>        
#include <pcf/ui/button/button.h> 
#include <pcf/ui/drawingview/drawingview.h>   
#include <pcf/graphics/context2d/context2d.h>
#include <pcf/graphics/pen/pen.h>  

pcf_ui_property_text text;
UiMainForm_t* mainform;
UiDrawingView_t* view;
UiControlInfo_t bcon={0};
UiButton_t* plot;

UiControlInfo_t errc = {0};
UiLabel_t* err;

UiControlInfo_t erric = {0};
UiLabel_t* erri;

struct pst{
    char flag, optr;
    double opnd;
};
struct pst* my_stru;

int xbgn, xend;
char move[50];
char xminmin[20], yminmin[20], xmaxmax[20], ymaxmax[20];

UiControlInfo_t xmincon = {0};
UiControlInfo_t xmaxcon = {0};
UiControlInfo_t ymincon = {0};
UiControlInfo_t ymaxcon = {0};
UiControlInfo_t exprcon = {0};

UiControlInfo_t xlcon = {0};
UiControlInfo_t ylcon = {0};
UiControlInfo_t xmcon = {0};
UiControlInfo_t ymcon = {0};
UiControlInfo_t xrcon = {0};
UiControlInfo_t yrcon = {0};

UiControlInfo_t xmovcon = {0};
UiControlInfo_t ymovcon = {0};
UiControlInfo_t xminimacon = {0};
UiControlInfo_t yminimacon = {0};
UiControlInfo_t xmaximacon = {0};
UiControlInfo_t ymaximacon = {0};

UiTextEdit_t* xmintxt;                                  
UiTextEdit_t* xmaxtxt;                                
UiTextEdit_t* ymintxt;
UiTextEdit_t* ymaxtxt;                                  
UiTextEdit_t* exprtxt;

UiTextEdit_t* xltxt;                                  
UiTextEdit_t* yltxt;                                  
UiTextEdit_t* xmtxt;                                  
UiTextEdit_t* ymtxt;                                  
UiTextEdit_t* xrtxt;                                  
UiTextEdit_t* yrtxt;  

UiTextEdit_t* xminimatxt;  
UiTextEdit_t* yminimatxt;  
UiTextEdit_t* xmaximatxt;  
UiTextEdit_t* ymaximatxt;  

UiTextEdit_t* xmovtxt;
UiTextEdit_t* ymovtxt;                                

double xminval, xmaxval, xstpval, yminval, ymaxval, xs, ys, x, y, xpting, ypting, stored[2][1500], xminima, yminima, xmaxima, ymaxima;

void PCF_STDCALL buttonClickHandler(UiButton_t* button);
void PCF_STDCALL DrawingViewPaintEventHandler(UiDrawingView_t* view, UiPaintEventArgs_t* e);
void PCF_STDCALL DrawingViewMouseClickEventHandler(const UiControl_t* sender, const UiControlMouseClickEventArgs_t* eventArgs);
void PCF_STDCALL DrawingViewMouseMoveEventHandler(const UiControl_t* sender, const UiControlMouseEventArgs_t* eventArgs);

void testGraphicsContext(UiMainForm_t* parentForm, int x, int y, int width, int height);
void addLabel(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text);
void addTextEdit(UiMainForm_t* mainform, UiControlInfo_t* info, UiTextEdit_t** edit, int x, int y, uint32_t width, uint32_t height, const char* text);
void addButton(UiMainForm_t* mainform, UiControlInfo_t* info, UiButton_t* button, int x, int y, uint32_t width, uint32_t height, const char* text);

int cmp(char* a, char* b);
char det_fun(char** strptr, struct pst* tmp, int **stss);
int pre(char c, struct pst* tmp, int **sts);
char detect(char* tmp_ptr);
void app_opnds(char** strptr, struct pst* c, int* i, int f);
struct pst* to_postfix(char* expression, int *sts); // allocates memory, free it after use
double eval_pst(struct pst* tmp, double xi, int *sts);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");

    //write your code after this line
    UiControlInfo_t info = {0};

    info.parent = NULL;
    info.typeId = enControlTypeIdValue_Mainform;
    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 600;
    info.size.Width  = 900;
    info.maximumSize.Height = 600;
    info.maximumSize.Width  = 900;
    info.minimumSize.Height = 600;
    info.minimumSize.Width  = 900;
    info.text = "Mathematical Expression Plotter";

    pcf_status_t status;
    mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;

        addLabel(mainform, 20, 15, 120, 20, "Enter expression below.");
        addLabel(mainform, 20, 33, 25, 20, " f(x) = ");
        addLabel(mainform, 703, 65, 60, 20, "X-minimum");
        addLabel(mainform, 703, 110, 100, 20, "X-maximum");
        addLabel(mainform, 703, 155, 60, 20, "Y-minimum");
        addLabel(mainform, 703, 200, 60, 20, "Y-maximum");

        addButton(mainform, &bcon, plot, 700, 30, 60, 20, "PLOT");
        addTextEdit(mainform, &xmincon, &xmintxt ,700, 80, 60, 20, "-5");
        addTextEdit(mainform, &xmaxcon, &xmaxtxt ,700, 125, 60, 20, "5");
        addTextEdit(mainform, &ymincon, &ymintxt ,700, 170, 60, 20, "-2");
        addTextEdit(mainform, &ymaxcon, &ymaxtxt ,700, 215, 60, 20, "2");
        addTextEdit(mainform, &exprcon, &exprtxt ,52, 30, 643, 20, "sin(x)");

        addLabel(mainform, 45, 510, 60, 20, "X-LEFT");
        addLabel(mainform, 125, 510, 60, 20, "Y-LEFT");
        addLabel(mainform, 285, 510, 60, 20, "X-POINTED");
        addLabel(mainform, 365, 510, 60, 20, "Y-POINTED");
        addLabel(mainform, 540, 510, 60, 20, "X-RIGHT");
        addLabel(mainform, 620, 510, 60, 20, "Y-RIGHT");
        addLabel(mainform, 310, 490, 150, 20, "POINTS ON CURVE");

        addLabel(mainform, 705, 240, 60, 20, "X-mouse");
        addLabel(mainform, 785, 240, 60, 20, "Y-mouse");

        addTextEdit(mainform, &xlcon, &xltxt ,30, 530, 70, 20, "");
        addTextEdit(mainform, &ylcon, &yltxt ,110, 530, 70, 20, "");
        addTextEdit(mainform, &xmcon, &xmtxt ,280, 530, 70, 20, "");
        addTextEdit(mainform, &ymcon, &ymtxt ,360, 530, 70, 20, "");
        addTextEdit(mainform, &xrcon, &xrtxt ,530, 530, 70, 20, "");
        addTextEdit(mainform, &yrcon, &yrtxt ,610, 530, 70, 20, "");

        addTextEdit(mainform, &xmovcon, &xmovtxt ,700, 260, 60, 20, "");
        addTextEdit(mainform, &ymovcon, &ymovtxt ,780, 260, 60, 20, "");

        addLabel(mainform, 700, 320, 200, 50, "DRAG OVER PEAKS TO ANALYZE");

        addLabel(mainform, 747, 345, 60, 20, "MINIMA");
        addLabel(mainform, 710, 360, 60, 20, "X-value");
        addLabel(mainform, 790, 360, 60, 20, "Y-value");

        addLabel(mainform, 747, 425, 60, 20, "MAXIMA");
        addLabel(mainform, 710, 440, 60, 20, "X-value");
        addLabel(mainform, 790, 440, 60, 20, "Y-value");

        addTextEdit(mainform, &xminimacon, &xminimatxt ,700, 380, 60, 20, "");
        addTextEdit(mainform, &yminimacon, &yminimatxt ,780, 380, 60, 20, "");
        addTextEdit(mainform, &xmaximacon, &xmaximatxt ,700, 460, 60, 20, "");
        addTextEdit(mainform, &ymaximacon, &ymaximatxt ,780, 460, 60, 20, "");

        UiControl_t* parent = pcf_ui_mainform_asControl(mainform);

        errc.parent = parent;
        errc.typeId = enControlTypeIdValue_Label;
        errc.size.Height = 20;
        errc.size.Width  = 120;
        errc.location.X = 780;
        errc.text = "ERROR HERE!";

        erric.parent = parent;
        erric.typeId = enControlTypeIdValue_Label;
        erric.size.Height = 20;
        erric.size.Width  = 620;
        erric.location.X = 52;
        erric.location.Y = 52;

        status = pcf_ui_mainform_run(mainform, &exitcode);
        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }
    return 0;
}


void PCF_STDCALL buttonClickHandler(UiButton_t* button) 
{

    pcf_ui_label_delete(err);
    pcf_ui_label_delete(erri);
    pcf_ui_drawingView_delete(view);

    int stat = 0;
    pcf_status_t st;

    UiControl_t* dum_con = pcf_ui_textedit_asControl(xmintxt);
    pcf_ui_control_getText(dum_con, text);
    fprintf(stdout, "  Xmin : %s\n", text);

    my_stru = to_postfix(text, &stat);
    if(stat){
        xminval = eval_pst(my_stru, 0, &stat);
        free(my_stru);

        if(stat){
            dum_con = pcf_ui_textedit_asControl(xmaxtxt);
            pcf_ui_control_getText(dum_con, text);
            fprintf(stdout, "  Xmax : %s\n", text);

            my_stru = to_postfix(text, &stat);
            if(stat){
                xmaxval = eval_pst(my_stru, 0, &stat);
                free(my_stru);
                if(stat){
                    if(xminval>xmaxval){
                        double temp = xminval;
                        xminval = xmaxval;
                        xmaxval = temp;
                    }
                    xstpval = (xmaxval-xminval)/1340;

                    dum_con = pcf_ui_textedit_asControl(ymintxt);
                    pcf_ui_control_getText(dum_con, text);
                    fprintf(stdout, "  Ymin : %s\n", text);

                    my_stru = to_postfix(text, &stat);
                    if(stat){
                        yminval = eval_pst(my_stru, 0, &stat);
                        free(my_stru);
                        if(stat){
                            dum_con = pcf_ui_textedit_asControl(ymaxtxt);
                            pcf_ui_control_getText(dum_con, text);
                            fprintf(stdout, "  Ymax : %s\n", text);

                            my_stru = to_postfix(text, &stat);
                            if(stat){
                                ymaxval = eval_pst(my_stru,0, &stat);
                                free(my_stru);
                                if(stat){
                                    if(yminval>ymaxval){
                                        double temp = yminval;
                                        yminval = ymaxval;
                                        ymaxval = temp;
                                    }
                                    dum_con = pcf_ui_textedit_asControl(exprtxt);
                                    pcf_ui_control_getText(dum_con, text);
                                    fprintf(stdout, "  f(x) : %s\n", text);

                                    my_stru = to_postfix(text, &stat);
                                    if(stat){
                                        eval_pst(my_stru,0, &stat);
                                        if(stat){
                                            testGraphicsContext(mainform, 20, 80, 673, 403);
                                        }
                                        else{
                                            fprintf(stdout, "Error while calculating equation!\n");
                                            erri = pcf_ui_label_new(&erric,&st);
                                            pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                                         }
                                     }
                                     else{
                                         fprintf(stdout, "Error while converting equation!\n");
                                         erri = pcf_ui_label_new(&erric,&st);
                                         pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                                     }
                                 }
                                 else{
                                     fprintf(stdout, "Error while calculating Ymax!\n");
                                     errc.location.Y = 217;
                                     err = pcf_ui_label_new(&errc,&st);
                                     erri = pcf_ui_label_new(&erric,&st);
                                     pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                                     pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
                                 }
                             }
                             else{
                                 fprintf(stdout, "Error while converting Ymax!\n");
                                 errc.location.Y = 217;
                                 err = pcf_ui_label_new(&errc,&st);
                                 erri = pcf_ui_label_new(&erric,&st);
                                 pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                                 pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
                             }
                         }
                         else{
                             fprintf(stdout, "Error while calculating Ymin!\n");
                             errc.location.Y = 172;
                             err = pcf_ui_label_new(&errc,&st);
                             erri = pcf_ui_label_new(&erric,&st);
                             pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                             pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
                         }
                     }
                     else{
                         fprintf(stdout, "Error while converting Ymin!\n");
                         errc.location.Y = 172;
                         err = pcf_ui_label_new(&errc,&st);
                         erri = pcf_ui_label_new(&erric,&st);
                         pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                         pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
                     }
                }
                else{
                    fprintf(stdout, "Error while calculating Xmax!\n");
                    errc.location.Y = 127;
                    err = pcf_ui_label_new(&errc,&st);
                    erri = pcf_ui_label_new(&erric,&st);
                    pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                    pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
                }
            }
            else{
                fprintf(stdout, "Error while converting Xmax!\n");
                errc.location.Y = 127;
                err = pcf_ui_label_new(&errc,&st);
                erri = pcf_ui_label_new(&erric,&st);
                pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
                pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
            }
        }
        else{
            fprintf(stdout, "Error while calculating Xmin!\n");
            errc.location.Y = 82;
            err = pcf_ui_label_new(&errc,&st);
            erri = pcf_ui_label_new(&erric,&st);
            pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
            pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
        }
    }
    else{
        fprintf(stdout, "Error while converting Xmin!\n");
        errc.location.Y = 82;
        err = pcf_ui_label_new(&errc,&st);
        erri = pcf_ui_label_new(&erric,&st);
        pcf_ui_label_setTextColor(erri, enGraphicsColorCode_Red);
        pcf_ui_label_setTextColor(err, enGraphicsColorCode_Red);
    }
}

void PCF_STDCALL DrawingViewPaintEventHandler(UiDrawingView_t* view, UiPaintEventArgs_t* e)
{

    pcf_status_t status;
    pcf_graphics_context2D_setSmoothingMode(e->context, enGraphicsContextSmoothingMode_HighestQuality);

    GraphicsPenConfig_t penConfig = {0};

    penConfig.color.value = ((0xFF << 24) | (0xFF) << 16 | (0x00 << 8) | (0x00));
    penConfig.endCap = enGraphicsLineCap_None;
    penConfig.startCap = enGraphicsLineCap_None;
    penConfig.lineJoinType = enGraphicsLineJoinType_Round;
    penConfig.style = enGraphicsPenStyle_Solid;
    penConfig.extra = NULL;
    penConfig.width = 1.0f;
    GraphicsPen_t* penR = pcf_graphics_pen_new(&penConfig, &status); 

    penConfig.color.value = ((0xFF << 24) | (0xFF << 0) | (0x00) | (0x00));
    GraphicsPen_t* penB = pcf_graphics_pen_new(&penConfig, &status);

    double xpp, ypp, xp, yp;
    xs=670/(xmaxval-xminval);
    ys=400/(ymaxval-yminval);

    status = pcf_graphics_context2D_drawLineF(e->context, penB, -xminval*xs, 0, -xminval*xs, 400);    // y-axis
    status = pcf_graphics_context2D_drawLineF(e->context, penB, 0, ymaxval*ys, 670, ymaxval*ys);      // x-axis
    pcf_graphics_pen_delete(penB);

    y = eval_pst(my_stru, xminval, &status);
    xp = 0;
    yp = (ymaxval-y)*ys;

    int i=0;
    for(x=xminval; x<=xmaxval; x+=xstpval){
        y = eval_pst(my_stru,x, &status);
        stored[0][i] = x;
        stored[1][i] = y;
        xpp = xp;
        ypp = yp;
        xp = (x-xminval)*xs;
        yp = (ymaxval-y)*ys;
        status = pcf_graphics_context2D_drawLine(e->context, penR, xpp, ypp, xp, yp);
        i++;
    }
    free(my_stru);
    pcf_graphics_pen_delete(penR);

    return;
}

void PCF_STDCALL DrawingViewMouseClickEventHandler(const UiControl_t* sender, const UiControlMouseClickEventArgs_t* eventArgs)
{
    xpting = xminval + (eventArgs->x)/xs;
    ypting = ymaxval - (eventArgs->y)/ys;
    if(eventArgs->down)
    {
        fprintf(stdout, "Mouse down at (X, Y) = (%lf, %lf)\n", xpting, ypting);
        xbgn = (eventArgs->x)*2;
    }
    else
    {
        fprintf(stdout, "Mouse up at (X, Y) = (%lf, %lf)\n", xpting, ypting);
        xend = (eventArgs->x)*2;

        if(xbgn>xend){
            int temp = xend;
            xend = xbgn;
            xbgn = temp;
        }

        xminima = stored[0][xbgn];
        yminima = stored[1][xbgn];
        xmaxima = stored[0][xbgn];
        ymaxima = stored[1][xbgn];

        for(int i=xbgn; i<=xend; i++){
            if(yminima > stored[1][i]){
                xminima = stored[0][i];
                yminima = stored[1][i];
            }
            if(ymaxima < stored[1][i]){
                xmaxima = stored[0][i];
                ymaxima = stored[1][i];
            }
        }

        pcf_ui_textedit_clear(xminimatxt);
        snprintf(xminmin, 20, "%lf", xminima);
        pcf_ui_textedit_insertTextAt(xminimatxt,0,xminmin);

        pcf_ui_textedit_clear(yminimatxt);
        snprintf(yminmin, 20, "%lf", yminima);
        pcf_ui_textedit_insertTextAt(yminimatxt,0,yminmin);

        pcf_ui_textedit_clear(xmaximatxt);
        snprintf(xmaxmax, 20, "%lf", xmaxima);
        pcf_ui_textedit_insertTextAt(xmaximatxt,0,xmaxmax);

        pcf_ui_textedit_clear(ymaximatxt);
        snprintf(ymaxmax, 20, "%lf", ymaxima);
        pcf_ui_textedit_insertTextAt(ymaximatxt,0,ymaxmax);


    }
}
void PCF_STDCALL DrawingViewMouseMoveEventHandler(const UiControl_t* sender, const UiControlMouseEventArgs_t* eventArgs)
{
    xpting = xminval + (eventArgs->x)/xs;
    ypting = ymaxval - (eventArgs->y)/ys;
    fprintf(stdout, "Mouse pointing at (X, Y) = (%lf, %lf)\n", xpting, ypting);

    snprintf(move, 50, "%lf", xpting);
    pcf_ui_textedit_clear(xmovtxt);
    pcf_ui_textedit_insertTextAt(xmovtxt,0,move);

    snprintf(move, 50, "%lf", ypting);
    pcf_ui_textedit_clear(ymovtxt);
    pcf_ui_textedit_insertTextAt(ymovtxt,0,move);

    snprintf(move, 50, "%lf",stored[0][(eventArgs->x)*2]);
    pcf_ui_textedit_clear(xmtxt);
    pcf_ui_textedit_insertTextAt(xmtxt,0,move);

    snprintf(move, 50, "%lf",stored[1][(eventArgs->x)*2]);
    pcf_ui_textedit_clear(ymtxt);
    pcf_ui_textedit_insertTextAt(ymtxt,0,move);

    snprintf(move, 50, "%lf",stored[0][(eventArgs->x)*2-1]);
    pcf_ui_textedit_clear(xltxt);
    pcf_ui_textedit_insertTextAt(xltxt,0,move);

    snprintf(move, 50, "%lf",stored[1][(eventArgs->x)*2-1]);
    pcf_ui_textedit_clear(yltxt);
    pcf_ui_textedit_insertTextAt(yltxt,0,move);

    snprintf(move, 50, "%lf",stored[0][(eventArgs->x)*2+1]);
    pcf_ui_textedit_clear(xrtxt);
    pcf_ui_textedit_insertTextAt(xrtxt,0,move);

    snprintf(move, 50, "%lf",stored[1][(eventArgs->x)*2+1]);
    pcf_ui_textedit_clear(yrtxt);
    pcf_ui_textedit_insertTextAt(yrtxt,0,move);

}

void testGraphicsContext(UiMainForm_t* parentForm, int x, int y, int width, int height)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(parentForm);
    if(parent)
    {
        UiControlInfo_t info = {0}; 
        info.parent = parent;
        info.typeId = enControlTypeIdValue_DrawingView;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "This is a dview";
        pcf_status_t status;
        view = pcf_ui_drawingView_new(&info,&status);
        if(view)
        {
            fprintf(stdout, "Created drawing view\n");
            status = pcf_ui_drawingView_attachPaintEventHandler(view, DrawingViewPaintEventHandler);
            if(status)
            {
                fprintf(stdout, "Failed while adding paint handler to drawing-view\n");
            }
            else
            {
                UiControl_t* drawingControl = pcf_ui_drawingView_asControl(view);
                pcf_ui_control_attachMouseClickEventHandler(drawingControl, DrawingViewMouseClickEventHandler);
                pcf_ui_control_attachMouseMoveEventHandler(drawingControl, DrawingViewMouseMoveEventHandler);
                fprintf(stdout, "Successfully added paint handler to drawing-view\n");
            }
        }
        else
        {
            fprintf(stdout, "Failed while creating drawing view\n");
        }
    }
    else
    {
        fprintf(stdout, "Failed while converting parent as control\n");
    }
}


void addLabel(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_Label;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = (char*)text;//Shall have text "What?"
        pcf_status_t status;

        UiLabel_t* label = pcf_ui_label_new(&info,&status);

        if(status)
        {
            fprintf(stdout, "Failed while creating label control, status = %d\n", status);
            return;
        }
        else
        {
            status = pcf_ui_label_setTextColor(label, enGraphicsColorCode_Black);

            if(status)
            {
                fprintf(stdout, "Failed while setting color for label control, status = %d\n", status);
            }
        }
    }
}

void addTextEdit(UiMainForm_t* mainform, UiControlInfo_t* info, UiTextEdit_t** edit, int x, int y, uint32_t width, uint32_t height, const char* text)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform); 
    if(parent)
    {
        info->parent = parent;
        info->typeId = enControlTypeIdValue_TextEdit;
        info->location.X = x;
        info->location.Y = y;
        info->size.Height = height;
        info->size.Width = width;
        info->text = (char*)text;//Shall have text "What?"
        pcf_status_t status;
        *edit = pcf_ui_textedit_new(info,&status); 
    }
}

void addButton(UiMainForm_t* mainform, UiControlInfo_t* info, UiButton_t* button, int x, int y, uint32_t width, uint32_t height, const char* text) {

    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    
    if(parent) {
        info->parent = parent;
        info->typeId = enControlTypeIdValue_SimpleButton;
        info->location.X = x;
        info->location.Y = y;
        info->size.Height = height;
        info->size.Width = width;
        info->text = (char*)text;
        pcf_status_t status;

        button = pcf_ui_button_new(info,&status);

        if(button == NULL) {
            fprintf(stdout, "Failed while creating button control, status = %d\n", status);
            return;
        } else {
            if(pcf_ui_button_attachClickedEventHandler(button, buttonClickHandler)) {
                fprintf(stdout, "Failed while adding button click handler\n");
                return;
            }
        }
    }
}

int cmp(char* a, char* b){
    while((*a!='('||*b!='\0')){
        if(*a!=*b)
            return 0;
        a++;
        b++;
        if(*a==' ')
            a++;
    }
    return 1;
}


char det_fun(char** strptr, struct pst* tmp, int **stss){

    char s[]="sin",  c[]="cos",  t[]="tan",  i[]="asin",  o[]="acos",  a[]="atan",  l[]="ln",   L[]="log";
    char S[]="sinh", C[]="cosh", T[]="tanh", I[]="asinh", O[]="acosh", A[]="atanh", q[]="sqrt", b[]="abs";
    char d[]="d",    u[]="u",    r[]="r",    p[]="p",     n[]="sgn",   $[]="sinc",  R[]="rect", v[]="tri";
    char m[]="min",  M[]="max",  h[]="hyp";

    if(cmp(*strptr,h)){
        (*strptr)+=3;
        return 'h';
    }
    if(cmp(*strptr,m)){
        (*strptr)+=3;
        return 'm';
    }
    if(cmp(*strptr,M)){
        (*strptr)+=3;
        return 'M';
    }
    if(cmp(*strptr,b)){
        (*strptr)+=3;
        return 'b';
    }
    if(cmp(*strptr,q)){
        (*strptr)+=4;
        return 'q';
    }
    if(cmp(*strptr,s)){
        (*strptr)+=3;
        return 's';
    }
    if(cmp(*strptr,c)){
        (*strptr)+=3;
        return 'c';
    }
    if(cmp(*strptr,t)){
        (*strptr)+=3;
        return 't';
    }
    if(cmp(*strptr,i)){
        (*strptr)+=4;
        return 'i';
    }
    if(cmp(*strptr,o)){
        (*strptr)+=4;
        return 'o';
    }
    if(cmp(*strptr,a)){
        (*strptr)+=4;
        return 'a';
    }
    if(cmp(*strptr,l)){
        (*strptr)+=2;
        return 'l';
    }
    if(cmp(*strptr,L)){
        (*strptr)+=3;
        return 'L';
    }
    if(cmp(*strptr,S)){
        (*strptr)+=4;
        return 'S';
    }
    if(cmp(*strptr,C)){
        (*strptr)+=4;
        return 'C';
    }
    if(cmp(*strptr,T)){
        (*strptr)+=4;
        return 'T';
    }
    if(cmp(*strptr,I)){
        (*strptr)+=5;
        return 'I';
    }
    if(cmp(*strptr,O)){
        (*strptr)+=5;
        return 'O';
    }
    if(cmp(*strptr,A)){
        (*strptr)+=5;
        return 'A';
    }
    if(cmp(*strptr,d)){
        (*strptr)++;
        return 'd';
    }
    if(cmp(*strptr,u)){
        (*strptr)++;
        return 'u';
    }
    if(cmp(*strptr,r)){
        (*strptr)++;
        return 'r';
    }
    if(cmp(*strptr,p)){
        (*strptr)++;
        return 'p';
    }
    if(cmp(*strptr,n)){
        (*strptr)+=3;
        return 'n';
    }
    if(cmp(*strptr,$)){
        (*strptr)+=4;
        return '$';
    }
    if(cmp(*strptr,R)){
        (*strptr)+=4;
        return 'R';
    }
    if(cmp(*strptr,v)){
        (*strptr)+=3;
        return 'v';
    }
    free(tmp);
    **stss=0;
    printf("\nSyntax Error!... Invalid input : \"");
    while(**strptr!='('&&**strptr!='\0'){
        printf("%c",**strptr);
        *strptr+=1;
    }
    printf("\"\n");
    return '?';
}

int pre(char c, struct pst* tmp, int **sts){
    switch(c){
        case '#': return 0;
        case '+':
        case '-': return 1;
        case '*':
        case '/':
        case '&':
        case '%': return 2;
        case '^': return 3;
        case '!': return 4;
        case 'b':
        case 'm':
        case 'M':
        case 'h':
        case 'q':
        case 's':
        case 'c':
        case 't':
        case 'i':
        case 'o':
        case 'a':
        case 'S':
        case 'C':
        case 'T':
        case 'I':
        case 'O':
        case 'A':
        case 'l':
        case 'L':
        case 'd':
        case 'u':
        case 'r':
        case 'p':
        case 'n':
        case '$':
        case 'R':
        case 'v': return 5;
        default : printf("\nError occured!\n %c is an invalid operator\n ", c);
                  erric.text="Invalid operator!";
                  **sts = 0;
                  free(tmp);
                  return '0';
    }
}

char detect(char* tmp_ptr){
    char c1=*(tmp_ptr+1), c0=*(tmp_ptr-1), c=*tmp_ptr;
    if((c>='0'&&c<='9')||c=='.'||c=='e'||(c=='p'&&c1=='i')||(c0=='p'&&c=='i'))
        return '.';
    else{
        if(c=='x')
            return 'x';
        else{
            switch(c){
                case '+':
                case '-':
                case '*':
                case '/':
                case '^':
                case '&':
                case '%': return '#';
                case 's':
                case 'c':
                case 't':
                case 'a':
                case 'l':
                case 'h':
                case 'm':
                case 'd':
                case 'u':
                case 'r':
                case 'p': return 'f';
                default : return '?';
            }
        }
    }
}

void app_opnds(char** strptr, struct pst* c, int* i, int f){
    double e=2.71828182845904523536, pi=3.14159265358979323846;
    char* endptr;
    int sgn;
    if(f)
        sgn=-1;
    else
        sgn=1;
    if(detect(*strptr+f)=='.'&&*(*strptr+f)!='e'&&*(*strptr+f)!='p'){
        (c+(++*i))->opnd = strtod(*strptr,&endptr);
        (c+*i)->flag = '.';
        *strptr=endptr;
    }
    else{
        if(*(*strptr+f)=='x'){
            (c+(++(*i)))->opnd = sgn*xminval;
            (c+*i)->flag = 'x';
            *strptr+=(f+1);
        }
        else{
            if(*(*strptr+f)=='e'){
                (c+(++(*i)))->opnd = sgn*e;
                (c+(*i))->flag = '.';
                *strptr+=(f+1);
            }
            else{
                if(*(*strptr+f)=='p'&&*(*strptr+f+1)=='i'){
                    (c+(++(*i)))->opnd = sgn*pi;
                    (c+*i)->flag = '.';
                    *strptr+=(f+2);
                }
            }
        }
    }
}

struct pst* to_postfix(char* expression, int *sts){
    char *strptr = expression, c, optr_stk[55];
    int pst_ptr=-1, stk_ptr=0, inc,n=5;
    struct pst *stru;
    stru = (struct pst*)malloc(10*sizeof(struct pst));
    if(stru==0){
        printf("Failed to allocate memory!\n");
        *sts = 0;
        return 0;
    }
    optr_stk[0]='#';
    if(*strptr=='-' && detect(strptr+1)=='.'){
        app_opnds(&strptr,stru,&pst_ptr,1);
    }
    if(*strptr=='-' &&(detect(strptr+1)=='f'||*(strptr+1)=='x'||*(strptr+1)=='(')){
        optr_stk[++stk_ptr]='!';
        strptr++;
    }
    while(*strptr!='\0'){
        inc=1;
        if(pst_ptr+7>=n){
            n+=10;
            stru = (struct pst*)realloc(stru,n*sizeof(struct pst));
            if(stru==0){
                printf("Insufficient memory!\n");
                *sts = 0;
                return 0;
            }
        }
        if(stk_ptr+2>55){
            printf("Stack overflow!\n");
            free(stru);
            *sts = 0;
            return 0;
        }
        if(*strptr=='-'&&(detect(strptr-1)=='#'||*(strptr-1)=='(')&&(detect(strptr+1)=='.')){
            app_opnds(&strptr,stru,&pst_ptr,1);
        }
        if(detect(strptr)=='.'||*(strptr)=='x'){
            app_opnds(&strptr,stru,&pst_ptr,0);
        }
        if(*strptr=='('){
             optr_stk[++stk_ptr]='(';
             strptr++;
        }
        if(*strptr==','){
            while(optr_stk[stk_ptr]!='('){
                if(stk_ptr<1){
                    printf("\nMissing '(' in input!\n");
                    erric.text="Missing '(' in input!";
                    free(stru);
                    *sts = 0;
                    return 0;
                }
                (stru+(++pst_ptr))->optr = optr_stk[stk_ptr--];
                (stru+pst_ptr)->flag = '#';
            }
            strptr++;
        }
        if(*strptr==')'){
            while(optr_stk[stk_ptr]!='('){
                if(stk_ptr<1){
                    printf("\nMissing '(' in input!\n");
                    erric.text="Parentheses unbalanced. Missing '(' in input!";
                    free(stru);
                    *sts = 0;
                    return 0;
                }
                (stru+(++pst_ptr))->optr = optr_stk[stk_ptr--];
                (stru+pst_ptr)->flag = '#';
            }
            stk_ptr--;
            strptr++;
        }
        while(*strptr==' ')
            strptr++;
        c=*strptr;
        if(detect(strptr)=='?'&&!(c=='\0'||c=='('||c==')'||c==','||c==' ')){
            printf("invalid input : %c\n",*strptr);
            erric.text="Invalid input";
            *sts = 0;
            free(stru);
            return 0;
        }
        if(!(c=='('||c==')'||c==','||c=='x'||c=='\0'||detect(strptr)=='.') && !((*(strptr-1)=='('||detect(strptr-1)=='#') && c=='-' && detect(strptr+1)=='.')){
            if(c=='-'&&(detect(strptr-1)=='#'||*(strptr-1)=='(')&&(detect(strptr+1)=='f'||*(strptr+1)=='x'||*(strptr+1)=='(')&&*(strptr-1)!=')'){
                c='!';
            }
            if(detect(strptr)!='#'){
                c=det_fun(&strptr, stru, &sts);
                inc=0;
            }
            while(optr_stk[stk_ptr]!='('){
                if(pre(c,stru,&sts)>pre(optr_stk[stk_ptr],stru,&sts)){
                    if(*sts==0)
                        return 0;
                    optr_stk[++stk_ptr]=c;
                }
                else{
                    (stru+(++pst_ptr))->optr = optr_stk[stk_ptr--];
                    (stru+pst_ptr)->flag = '#';
                    continue;
                }
                strptr+=inc;
                c=*strptr;
                break;
            }
            if(optr_stk[stk_ptr]=='('){
                optr_stk[++stk_ptr]=c;
                strptr+=inc;
            }
        }
    }
    while(stk_ptr>0){
        if(optr_stk[stk_ptr]=='('){
            printf("\nParentheses unbalanced\nMissing ')' in input\n");
            erric.text="Parentheses unbalanced. Missing ')' in input";
            free(stru);
            *sts = 0;
            return 0;
        }
        if(pst_ptr+7>=n){
            n+=10;
            stru = (struct pst*)realloc(stru,n*sizeof(struct pst));
            if(stru==0){
                printf("Insufficient memory!\n");
                *sts = 0;
                return 0;
            }
        }
        (stru+(++pst_ptr))->optr = optr_stk[stk_ptr--];
        (stru+(pst_ptr))->flag='#';
    }
    (stru+(++pst_ptr))->flag='\0';
    *sts = 1;
    return stru;
}

double eval_pst(struct pst* tmp, double xi, int *sts){
    struct pst* rep=tmp;
    double opnd_stk[55], a, b;
    int stk_ptr=-1;
    while(rep->flag!='\0'){
        if(rep->flag=='x')
            (rep->opnd) = xi;
        rep++;
    }
    rep=tmp;
    while((tmp->flag)!='\0'){
        if(stk_ptr+2>55){
            printf("Stack overflow!\n");
            free(rep);
            *sts=0;
            return 0;
        }
        if(tmp->flag=='.'||tmp->flag=='x'){
            opnd_stk[++stk_ptr]=(tmp++)->opnd;
        }
        if(tmp->flag=='#'){
            switch(tmp->optr){
                case '+': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = a+b;
                          break;
                case '-': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = a-b;
                          break;
                case '*': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = a*b;
                          break;
                case '/': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = a/b;
                          break;
                case '&': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = (int)(a/b);
                          break;
                case '%': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = fmod(a,b);
                          break;
                case '^': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = pow(a,b);
                          break;
                case '!': opnd_stk[stk_ptr] = -opnd_stk[stk_ptr];
                          break;
                case 'b': opnd_stk[stk_ptr] = fabs(opnd_stk[stk_ptr]);
                          break;
                case 'q': opnd_stk[stk_ptr] = sqrt(opnd_stk[stk_ptr]);
                          break;
                case 'm': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          if(b<a)
                              opnd_stk[stk_ptr] = b;
                          else
                              opnd_stk[stk_ptr] = a;
                          break;
                case 'M': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          if(b>a)
                              opnd_stk[stk_ptr] = b;
                          else
                              opnd_stk[stk_ptr] = a;
                          break;
                case 'h': b = opnd_stk[stk_ptr--];
                          a = opnd_stk[stk_ptr];
                          opnd_stk[stk_ptr] = sqrt(b*b+a*a);
                          break;
                case 's': opnd_stk[stk_ptr] = sin(opnd_stk[stk_ptr]);
                          break;
                case 'c': opnd_stk[stk_ptr] = cos(opnd_stk[stk_ptr]);
                          break;
                case 't': opnd_stk[stk_ptr] = tan(opnd_stk[stk_ptr]);
                          break;
                case 'i': opnd_stk[stk_ptr] = asin(opnd_stk[stk_ptr]);
                          break;
                case 'o': opnd_stk[stk_ptr] = acos(opnd_stk[stk_ptr]);
                          break;
                case 'a': opnd_stk[stk_ptr] = atan(opnd_stk[stk_ptr]);
                          break;
                case 'S': opnd_stk[stk_ptr] = sinh(opnd_stk[stk_ptr]);
                          break;
                case 'C': opnd_stk[stk_ptr] = cosh(opnd_stk[stk_ptr]);
                          break;
                case 'T': opnd_stk[stk_ptr] = tanh(opnd_stk[stk_ptr]);
                          break;
                case 'I': opnd_stk[stk_ptr] = asinh(opnd_stk[stk_ptr]);
                          break;
                case 'O': opnd_stk[stk_ptr] = acosh(opnd_stk[stk_ptr]);
                          break;
                case 'A': opnd_stk[stk_ptr] = atanh(opnd_stk[stk_ptr]);
                          break;
                case 'l': opnd_stk[stk_ptr] = log(opnd_stk[stk_ptr]);
                          break;
                case 'L': opnd_stk[stk_ptr] = log10(opnd_stk[stk_ptr]);
                          break;
                case 'd': if(opnd_stk[stk_ptr]==0.0)
                              opnd_stk[stk_ptr]=1;
                          else
                              opnd_stk[stk_ptr]=0;
                          break;
                case 'u': if(opnd_stk[stk_ptr]>=0)
                              opnd_stk[stk_ptr]=1;
                          else
                              opnd_stk[stk_ptr]=0;
                          break;
                case 'r': if(opnd_stk[stk_ptr]<0)
                              opnd_stk[stk_ptr]=0;
                          break;
                case 'p': a = opnd_stk[stk_ptr];
                          if(a<=0)
                              opnd_stk[stk_ptr]=0;
                          else
                              opnd_stk[stk_ptr]=a*a/2;
                          break;
                case 'n': a = opnd_stk[stk_ptr];
                          if(a<0)
                              opnd_stk[stk_ptr]=-1;
                          else{
                              if(a>0)
                                  opnd_stk[stk_ptr]=1;
                          }
                          break;
                case '$': a = opnd_stk[stk_ptr];
                          if(a==0)
                              opnd_stk[stk_ptr]=1;
                          else
                              opnd_stk[stk_ptr]=sin(a)/a;
                          break;
                case 'R': a = fabs(opnd_stk[stk_ptr]);
                          if(a<=0.5)
                              opnd_stk[stk_ptr]=1;
                          else
                              opnd_stk[stk_ptr]=0;
                          break;
                case 'v': a = fabs(opnd_stk[stk_ptr]);
                          if(a<1)
                              opnd_stk[stk_ptr]=1-a;
                          else
                              opnd_stk[stk_ptr]=0;
                          break;
                default : printf("\nInvalid operator : %c\n",tmp->optr);
                          erric.text="Invalid input";
                          return 0;
            }
            tmp++;
        }
    }
    if(stk_ptr>0){
        printf("\nError! Too many operands! [or less operators!]\n");
        free(rep);
        erric.text="Error! Too many operands! [or less operators!]";
        *sts=0;
        return 0;
    }
    if(stk_ptr<0){
        printf("\nError! Too many operators! [or less operands!]\n");
        free(rep);
        erric.text="Error! Too many operators! [or less operands!]";
        *sts=0;
        return 0;
    }
    *sts=1;
    return opnd_stk[0];
}


